package edu.austral.dissis.starship.base.model.constants;

public enum Action {
    FORWARD,
    BACKWARDS,
    ROTATE_LEFT,
    ROTATE_RIGHT,
    SHOOT,
    MOVE_RIGHT,
    MOVE_LEFT,
    CHANGE_WEAPON;
}
