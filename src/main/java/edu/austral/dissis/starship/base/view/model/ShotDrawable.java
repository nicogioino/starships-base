package edu.austral.dissis.starship.base.view.model;


import edu.austral.dissis.starship.base.controller.GameController;
import edu.austral.dissis.starship.base.model.Shot;
import edu.austral.dissis.starship.base.model.constants.Configs;
import edu.austral.dissis.starship.base.model.constants.Image;
import edu.austral.dissis.starship.base.vector.Vector2;
import edu.austral.dissis.starship.base.view.DrawableEntity;
import edu.austral.dissis.starship.base.view.GameView;
import processing.core.PGraphics;

public class ShotDrawable implements DrawableEntity {

    private final Shot shot;
    private final Image image;
    private final int bulletWidth;
    private final int bulletHeight;

    public ShotDrawable(Shot shot, Image image, int bulletWidth, int bulletHeight) {
        this.shot = shot;
        this.image = image;
        this.bulletWidth = bulletWidth;
        this.bulletHeight = bulletHeight;
    }


    @Override
    public void updateView(PGraphics graphics) {
        if (!shot.isAlive()) {
            GameView.addDrawableToRemove(this);
            removeEntity();
        }
        else {
            shot.updatePosition();
            graphics.image(GameView.getPImage(getIMAGE()),
                    getPosition().getX() + ((Configs.STARSHIP_WIDTH - bulletWidth) >> 1),
                    getPosition().getY(), bulletWidth, bulletHeight);
        }
    }

    @Override
    public Image getIMAGE() {
        return this.image;
    }

    @Override
    public Vector2 getPosition() {
        return shot.getPosition();
    }

    @Override
    public Vector2 getDirection() {
        return shot.getDirection();
    }

    @Override
    public Float getSpeed() {
        return shot.getSpeed();
    }

    @Override
    public void removeEntity() {
        GameController.removeCollisionable(this.shot);
    }
}
