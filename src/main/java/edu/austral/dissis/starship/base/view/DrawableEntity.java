package edu.austral.dissis.starship.base.view;

import edu.austral.dissis.starship.base.model.constants.Image;
import edu.austral.dissis.starship.base.vector.Vector2;
import processing.core.PGraphics;




public interface DrawableEntity extends Drawable{

    void updateView(PGraphics graphics);

    Image getIMAGE();

    Vector2 getPosition();

    Vector2 getDirection();

    Float getSpeed();

    void removeEntity();

}
