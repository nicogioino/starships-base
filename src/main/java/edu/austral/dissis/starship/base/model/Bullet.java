package edu.austral.dissis.starship.base.model;


import edu.austral.dissis.starship.base.model.constants.Image;
import edu.austral.dissis.starship.base.vector.Vector2;

public abstract class Bullet {

    private final Image image;
    private final int width;
    private final int height;
    private final int strength;

    public Bullet(Image image, int width, int height, int strength) {
        this.image = image;
        this.width = width;
        this.height = height;
        this.strength = strength;
    }

    public Image getImage() {
        return image;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getStrength() {
        return strength;
    }

    public abstract void shot(Player player, Vector2 position, Vector2 direction);
}
