package edu.austral.dissis.starship.base.model;

public enum CollisionableType {
    ASTEROID, SHOT, STARSHIP
}
