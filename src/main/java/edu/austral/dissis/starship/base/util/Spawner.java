package edu.austral.dissis.starship.base.util;


import edu.austral.dissis.starship.base.controller.GameController;
import edu.austral.dissis.starship.base.model.Asteroid;
import edu.austral.dissis.starship.base.model.constants.Configs;
import edu.austral.dissis.starship.base.vector.Vector2;
import edu.austral.dissis.starship.base.view.GameView;
import edu.austral.dissis.starship.base.view.model.AsteroidDrawable;

import java.util.Random;

public class Spawner {

    private static final Random random = new Random();

    public void spawnAsteroid() {
        int width = getRandomWidth();
        Asteroid asteroid = new Asteroid(getRandomPosition(width),
                getRandomDirection(),
                getRandomSpeed(),
                width);
        GameController.addCollisionable(asteroid);
        GameView.addDrawable(new AsteroidDrawable(asteroid));
    }

    private int getRandomWidth() {
        return Configs.ASTEROID_WIDTH + random.nextInt(80);
    }

    private float getRandomSpeed() {
        return Configs.ASTEROID_MIN_SPEED + random.nextFloat() * (Configs.ASTEROID_MAX_SPEED - Configs.ASTEROID_MIN_SPEED);
    }

    private Vector2 getRandomPosition(int width) {
        Vector2 position;
        int r = random.nextInt(4);
        if (r == 0 || r == 1){
            position = Vector2.vector(r == -width ? 0 : Configs.WINDOW_WIDTH + width, random.nextInt(Configs.WINDOW_HEIGHT));
        }
        else {
            position = Vector2.vector(random.nextInt(Configs.WINDOW_WIDTH), r == 2 ? -width : Configs.WINDOW_HEIGHT + width);
        }
        return position;
    }

    private Vector2 getRandomDirection() {
        float rx = random.nextFloat();
        float ry = random.nextFloat();
        float min = -1;
        float max = 1;

        return Vector2.vector(min + rx * (max - min), min + ry * (max - min));
    }
}
