package edu.austral.dissis.starship.base.view;

import processing.core.PGraphics;

public interface Drawable {

    void updateView(PGraphics graphics);

}
