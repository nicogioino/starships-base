package edu.austral.dissis.starship.base.model;


import edu.austral.dissis.starship.base.vector.Vector2;

public abstract class Weapon {

    private final Bullet bullet;
    private Vector2 position;
    private Vector2 direction;

    public Weapon(Bullet bullet) {
        this.bullet = bullet;
    }

    public Bullet getBullet() {
        return bullet;
    }

    public void shoot(Player player) {
        bullet.shot(player, position, direction);
    }

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }

    public Vector2 getDirection() {
        return direction;
    }

    public void setDirection(Vector2 direction) {
        this.direction = direction;
    }

}
