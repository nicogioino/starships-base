package edu.austral.dissis.starship.base.model.constants;

public enum Image {
    BACKGROUND,
    STARSHIP,
    ASTEROID,
    MISSILE_BULLET,
    LASER_BULLET,
    GAME_OVER,
    LIVES
}
