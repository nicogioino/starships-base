package edu.austral.dissis.starship.base.collision;

import edu.austral.dissis.starship.base.model.CollisionableType;

import java.awt.*;

public interface Collisionable<T extends Collisionable<T>> {
    Shape getShape();

    void collisionedWith(T collisionable);

    CollisionableType getType();
}
