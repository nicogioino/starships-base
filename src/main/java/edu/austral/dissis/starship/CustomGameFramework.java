package edu.austral.dissis.starship;

import edu.austral.dissis.starship.base.controller.GameController;
import edu.austral.dissis.starship.base.framework.GameFramework;
import edu.austral.dissis.starship.base.framework.ImageLoader;
import edu.austral.dissis.starship.base.framework.WindowSettings;
import edu.austral.dissis.starship.base.model.constants.Configs;
import edu.austral.dissis.starship.base.model.constants.Image;
import edu.austral.dissis.starship.base.view.GameView;
import processing.core.PConstants;
import processing.core.PGraphics;
import processing.core.PImage;
import processing.event.KeyEvent;

import java.util.Set;

public class CustomGameFramework implements GameFramework {


    private final GameController gameController = new GameController();
    private final GameView gameView = new GameView();


    @Override
    public void setup(WindowSettings windowsSettings, ImageLoader imageLoader) {
        windowsSettings
                .setSize(Configs.WINDOW_WIDTH, Configs.WINDOW_HEIGHT);

        gameView.addPImage(Image.STARSHIP, imageLoader.load("images/x-wing.png"));
        gameView.addPImage(Image.ASTEROID, imageLoader.load("images/tie-fighter.png"));
        gameView.addPImage(Image.LASER_BULLET, imageLoader.load("images/laser_sprite.png"));
        gameView.addPImage(Image.MISSILE_BULLET, imageLoader.load("images/missile_sprite.png"));
        gameView.addPImage(Image.GAME_OVER, imageLoader.load("images/darth-over.jpg"));

        gameController.createPlayer();
    }

    @Override
    public void draw(PGraphics graphics, float timeSinceLastDraw, Set<Integer> keySet) {
        if (gameController.playerAlive()) {
            keySet.forEach(gameController::onKeyPressedEvent);
            gameController.updateGame();
            gameView.render(graphics);
        }
        else {
            gameOver(graphics);
        }

    }

    private void gameOver(PGraphics graphics) {
        gameController.gameOver();
        gameView.gameOver(graphics);
    }

    @Override
    public void keyPressed(KeyEvent event) {
        //gameController.onKeyPressedEvent(event.getKeyCode());
    }

    @Override
    public void keyReleased(KeyEvent event) {
        gameController.onReleased(event);
    }

}

